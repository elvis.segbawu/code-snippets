# TT-Labs Foundations
see the following sections

- [Algorithms and Data Structures](DSA/) &#10004;
- [Databases](DB/) &#10004;
- [Java](JAVA/) &#10004;
- [Python](PYTHON/) &#10004;
- [Version Control with Git](GIT/) &#10004;
- [Agile](AGILE/) &#10004;
- [Object-Oriented Programming](OOP/) &#10004;
- [Scala](SCALA/) <img src="images/loader.gif" width="10" height="10"/>
- [Functional Programming](FUNC/)  <img src="images/loader.gif" width="10" height="10"/>
- [Test-Driven Development](TDD/)  &#10004;
- [TypeScript](TS/)
- [Angular](ANG/)
